Installation:
=============
1. Install python virtualenv and virtualenvwrapper:
    - for linux(debian/ubuntu) use:
        - `$ sudo apt-get install python-pip python-dev build-essential` 
        - `$ sudo pip install --upgrade pip`
        - `$ sudo pip install --upgrade virtualenv`
        - `$ sudo pip install --upgrade virtualenvwrapper`
    - for windows use this site [http://www.lfd.uci.edu/~gohlke/pythonlibs/](http://www.lfd.uci.edu/~gohlke/pythonlibs/) for installing pip and virtualenv
    and `pip install virtualenvwrapper-win` for installing wrapper.
2. Install project requiremets use `pip install -r requirements.txt` inthe project's root folder.
