# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import
import sys
import os
# import re
# from pprint import pprint

try:
    import json
except ImportError:
    sys.exit("You need to have json module installed")

from grab import Grab

# Lil hack
sys.path.append(os.path.sep.join(os.path.abspath(__file__).split(os.path.sep)[:-3]))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "lowcost_monitor.settings")

from lowcost_monitor.flyrequest.models import *

#TODO: �������� �� ��� ����� ����������� � ��������� ����������

AEROPORT_CODE_KEY_NAME = 'IATA'
AEROPORT_NAME_KEY_NAME = 'Name'
AEROPORT_SHORT_NAME_KEY_NAME = 'ShortName'

RELATION_AEROPORT_KEY_NAME = 'Station'
RELATION_DESTINITIONS_KEY_NAME = 'Code'


LOWCOST_SITENAME = 'wizzair'

LOWCOST_SITE_OBJ = LowcostSite.objects.get(name=LOWCOST_SITENAME)

WIZZAIR_URLS = {
    'start_ua':'http://wizzair.com/uk-UA/Search',
    'start_en':'http://wizzair.com/en-GB/Search'
}

AEROPORT_TOKENS = {
    'start':u'wizzAutocomplete.STATION =',
    'mid':u';wizzAutocomplete.MARKETINFO = ',
    'end':u';wizzAutocomplete.MACINFO'
}

def get_aeroports_and_relations():
    """������� ������� ������� javascript � ������� � ���������� � �������� ����� ����"""

    g = Grab()
    g.go(WIZZAIR_URLS['start_ua'])

    # ��� �� ����� ���� ��� �������
    assert g.search(AEROPORT_TOKENS['start'])
    assert g.search(AEROPORT_TOKENS['end'])

    # ������� �������
    tsi = g.response.body.index(AEROPORT_TOKENS['start'].encode('utf-8'))
    tei = g.response.body.index(AEROPORT_TOKENS['end'].encode('utf-8'))

    # ��������� raw ������ ����������� � javascript �������
    loaded_json = g.response.body[tsi:tei]

    # ������� ��������� �����
    loaded_json = loaded_json.replace(AEROPORT_TOKENS['start'].encode('utf-8'), '')

    # � ��� ����������� �� ���� ������, � ���. ��� ��� ��������� ��
    loaded_json = loaded_json.split(AEROPORT_TOKENS['mid'].encode('utf-8'))

    assert len(loaded_json)

    # ����� ������ ���� �������� json, � �� ������ ��� extra data
    AEROPORTinations_relations = json.loads(loaded_json[1]) + json.loads(loaded_json[0])
    
    codes = [code for code in AEROPORTinations_relations if len(code) == 6]
    relations = [rel for rel in AEROPORTinations_relations if len(rel) == 2]
    
    return codes, relations

def update_aeroports(codes):
    """������� ���������� ����� ���������� � ������� �� �����"""
    
    for code in codes:
        if not code[AEROPORT_CODE_KEY_NAME]: continue

        try:
            obj = Aeroport.objects.get(lowcost_site = LOWCOST_SITE_OBJ,
                                        code = code[AEROPORT_CODE_KEY_NAME])
            obj.    
        except Aeroport.DoesNotExist:
            Aeroport.objects.create(
                            lowcost_site = LOWCOST_SITE_OBJ,
                            code = code[AEROPORT_CODE_KEY_NAME],
                            name = code[AEROPORT_NAME_KEY_NAME],
                            short_name = code[AEROPORT_SHORT_NAME_KEY_NAME]
                           ).save()
            #TODO: �����������

def update_aeroports_relations(relations):
    """������� ���������� ������ ����� �����������"""
    
    get_aeroport = lambda c: Aeroport.objects.get(lowcost_site=LOWCOST_SITE_OBJ,
                                                  code=c)

    map(
        lambda i: i.save(),
        [AeroportsRelations(from_dest = get_aeroport(r[RELATION_AEROPORT_KEY_NAME]), 
                            can_fly_to = get_aeroport(j)) for r in relations 
                                                          for j in r[RELATION_DESTINITIONS_KEY_NAME]]
    )


def task_update_aeroports_and_their_relations():
    """������� ���������� ���������� � ������ ����� ����"""

    codes, relations = get_aeroports_and_relations()
    
    update_aeroports(codes)
    update_aeroports_relations(relations)
    
def get_viewstate():
    """������� ������� viewstate ������� ������"""

    g = Grab()
    #g.setup(proxy='proxy.pib.com.ua:3128', proxy_type='http')
    g.go(WIZZAIR_URLS['start_ua'])

    # 
    VIEWSTATE = g.xpath_list('*//input [@id ="viewState"]/@value')
    return VIEWSTATE[0]
    
   


def task_fech_flies():
    # ��������:
    # 1. ������� �� �������
    #  2. ������� � ���� �������� __VIEWSTATE
    #  3. ��������� ������� ��� ����� ����������������� �������
    #  4. grab.post
    post = {
        # ����� �������� �����
        '_EVENTTARGET':'ControlGroupRibbonAnonHomeView_AvailabilitySearchInputRibbonAnonHomeView_ButtonSubmit',
        # ��������� ����������, ����� �������� �� ��������
        '__VIEWSTATE': get_viewstate(),
        # ������?
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$OriginStation':'IEV',
        # ����?
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$DestinationStation':'AYT',
        # �����?
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$DepartureDate':'17.10.2012',
        # ������� ��������?
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$PaxCountADT':'1',
        # ������� �����?
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$PaxCountCHD':'0',
        # ��� ��� ������� �����?
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$PaxCountINFANT':'0',
        # �����
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$BaggageCount':'0',
        # ������?
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$WizzFlightSearchInput2_COUNTRIES':'UA',
        # ������ �������� ������
        'ControlGroupRibbonAnonHomeView$AvailabilitySearchInputRibbonAnonHomeView$ButtonSubmit':'������'
    }


tasks = [v for k,v in globals().items() if k.startswith('task_')]

if __name__ == '__main__':
    for task in tasks: task()