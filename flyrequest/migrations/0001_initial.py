# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'LowcostSite'
        db.create_table('flyrequest_lowcostsite', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal('flyrequest', ['LowcostSite'])

        # Adding model 'Aeroport'
        db.create_table('flyrequest_aeroport', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lowcost_site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['flyrequest.LowcostSite'])),
            ('dest_code', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('dest_name', self.gf('django.db.models.fields.CharField')(max_length=125)),
        ))
        db.send_create_signal('flyrequest', ['Aeroport'])

        # Adding model 'AeroportsRelations'
        db.create_table('flyrequest_aeroportsrelations', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_dest', self.gf('django.db.models.fields.related.ForeignKey')(related_name='aeroport', to=orm['flyrequest.Aeroport'])),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('flyrequest', ['AeroportsRelations'])

        # Adding M2M table for field can_fly_to on 'AeroportsRelations'
        db.create_table('flyrequest_aeroportsrelations_can_fly_to', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('aeroportsrelations', models.ForeignKey(orm['flyrequest.aeroportsrelations'], null=False)),
            ('aeroport', models.ForeignKey(orm['flyrequest.aeroport'], null=False))
        ))
        db.create_unique('flyrequest_aeroportsrelations_can_fly_to', ['aeroportsrelations_id', 'aeroport_id'])


    def backwards(self, orm):
        # Deleting model 'LowcostSite'
        db.delete_table('flyrequest_lowcostsite')

        # Deleting model 'Aeroport'
        db.delete_table('flyrequest_aeroport')

        # Deleting model 'AeroportsRelations'
        db.delete_table('flyrequest_aeroportsrelations')

        # Removing M2M table for field can_fly_to on 'AeroportsRelations'
        db.delete_table('flyrequest_aeroportsrelations_can_fly_to')


    models = {
        'flyrequest.aeroport': {
            'Meta': {'ordering': "['dest_code', 'dest_name']", 'object_name': 'Aeroport'},
            'dest_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'dest_name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lowcost_site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['flyrequest.LowcostSite']"})
        },
        'flyrequest.aeroportsrelations': {
            'Meta': {'object_name': 'AeroportsRelations'},
            'can_fly_to': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'destinations'", 'symmetrical': 'False', 'to': "orm['flyrequest.Aeroport']"}),
            'from_dest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aeroport'", 'to': "orm['flyrequest.Aeroport']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        'flyrequest.lowcostsite': {
            'Meta': {'object_name': 'LowcostSite'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['flyrequest']