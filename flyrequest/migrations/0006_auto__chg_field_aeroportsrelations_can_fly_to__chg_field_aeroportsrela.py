# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'AeroportsRelations.can_fly_to'
        db.alter_column('flyrequest_aeroportsrelations', 'can_fly_to_id', self.gf('django.db.models.fields.related.ForeignKey')(to_field='code', to=orm['flyrequest.Aeroport']))

        # Changing field 'AeroportsRelations.from_dest'
        db.alter_column('flyrequest_aeroportsrelations', 'from_dest_id', self.gf('django.db.models.fields.related.ForeignKey')(to_field='code', to=orm['flyrequest.Aeroport']))

    def backwards(self, orm):

        # Changing field 'AeroportsRelations.can_fly_to'
        db.alter_column('flyrequest_aeroportsrelations', 'can_fly_to_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['flyrequest.Aeroport']))

        # Changing field 'AeroportsRelations.from_dest'
        db.alter_column('flyrequest_aeroportsrelations', 'from_dest_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['flyrequest.Aeroport']))

    models = {
        'flyrequest.aeroport': {
            'Meta': {'ordering': "['code', 'name']", 'object_name': 'Aeroport'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lowcost_site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['flyrequest.LowcostSite']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '125'})
        },
        'flyrequest.aeroportsrelations': {
            'Meta': {'object_name': 'AeroportsRelations'},
            'can_fly_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'destination'", 'to_field': "'code'", 'to': "orm['flyrequest.Aeroport']"}),
            'from_dest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aeroport'", 'to_field': "'code'", 'to': "orm['flyrequest.Aeroport']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        'flyrequest.lowcostsite': {
            'Meta': {'object_name': 'LowcostSite'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['flyrequest']