# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Aeroport.dest_short_name'
        db.add_column('flyrequest_aeroport', 'dest_short_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=125),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Aeroport.dest_short_name'
        db.delete_column('flyrequest_aeroport', 'dest_short_name')


    models = {
        'flyrequest.aeroport': {
            'Meta': {'ordering': "['dest_code', 'dest_name']", 'object_name': 'Aeroport'},
            'dest_code': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'dest_name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'dest_short_name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lowcost_site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['flyrequest.LowcostSite']"})
        },
        'flyrequest.aeroportsrelations': {
            'Meta': {'object_name': 'AeroportsRelations'},
            'can_fly_to': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'destinations'", 'symmetrical': 'False', 'to': "orm['flyrequest.Aeroport']"}),
            'from_dest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aeroport'", 'to': "orm['flyrequest.Aeroport']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        'flyrequest.lowcostsite': {
            'Meta': {'object_name': 'LowcostSite'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['flyrequest']