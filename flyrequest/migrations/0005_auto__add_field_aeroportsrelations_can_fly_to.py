# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'AeroportsRelations.can_fly_to'
        db.add_column('flyrequest_aeroportsrelations', 'can_fly_to',
                      self.gf('django.db.models.fields.related.ForeignKey')(default='', related_name='destination', to=orm['flyrequest.Aeroport']),
                      keep_default=False)

        # Removing M2M table for field can_fly_to on 'AeroportsRelations'
        db.delete_table('flyrequest_aeroportsrelations_can_fly_to')


    def backwards(self, orm):
        # Deleting field 'AeroportsRelations.can_fly_to'
        db.delete_column('flyrequest_aeroportsrelations', 'can_fly_to_id')

        # Adding M2M table for field can_fly_to on 'AeroportsRelations'
        db.create_table('flyrequest_aeroportsrelations_can_fly_to', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('aeroportsrelations', models.ForeignKey(orm['flyrequest.aeroportsrelations'], null=False)),
            ('aeroport', models.ForeignKey(orm['flyrequest.aeroport'], null=False))
        ))
        db.create_unique('flyrequest_aeroportsrelations_can_fly_to', ['aeroportsrelations_id', 'aeroport_id'])


    models = {
        'flyrequest.aeroport': {
            'Meta': {'ordering': "['code', 'name']", 'object_name': 'Aeroport'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lowcost_site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['flyrequest.LowcostSite']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '125'})
        },
        'flyrequest.aeroportsrelations': {
            'Meta': {'object_name': 'AeroportsRelations'},
            'can_fly_to': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'destination'", 'to': "orm['flyrequest.Aeroport']"}),
            'from_dest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aeroport'", 'to': "orm['flyrequest.Aeroport']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        'flyrequest.lowcostsite': {
            'Meta': {'object_name': 'LowcostSite'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['flyrequest']