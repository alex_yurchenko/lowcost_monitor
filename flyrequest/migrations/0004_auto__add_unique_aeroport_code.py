# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Aeroport', fields ['code']
        db.create_unique('flyrequest_aeroport', ['code'])


    def backwards(self, orm):
        # Removing unique constraint on 'Aeroport', fields ['code']
        db.delete_unique('flyrequest_aeroport', ['code'])


    models = {
        'flyrequest.aeroport': {
            'Meta': {'ordering': "['code', 'name']", 'object_name': 'Aeroport'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lowcost_site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['flyrequest.LowcostSite']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '125'}),
            'short_name': ('django.db.models.fields.CharField', [], {'max_length': '125'})
        },
        'flyrequest.aeroportsrelations': {
            'Meta': {'object_name': 'AeroportsRelations'},
            'can_fly_to': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'destinations'", 'symmetrical': 'False', 'to': "orm['flyrequest.Aeroport']"}),
            'from_dest': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'aeroport'", 'to': "orm['flyrequest.Aeroport']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {})
        },
        'flyrequest.lowcostsite': {
            'Meta': {'object_name': 'LowcostSite'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['flyrequest']