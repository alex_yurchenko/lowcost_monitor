# -*- coding: utf-8 -*- 

import datetime

from django.db import models
from django.conf import settings
from django.utils.timezone import utc

SPIDERS_PATH = getattr(settings, 'SPIDERS_PATH')

class LowcostSite(models.Model):
    u"""Модель для сайта lowcost."""

    url = models.URLField()
    name = models.CharField(max_length=64)

# class Flyrequest(models.Model):
#     u"""Модель запроса наблюдения за рейсом.
#     Используется для получения от пользователя рейса для наблюдения.
#     """

#     lowcost_site = models.ForeignKey(LowcostSiteSpider)
#     date_of_departure = models.DateField()
#     return_date = models.DateField(null=True, blank=True)
#     number_of_adults = models.IntegerField(null=True, blank=True)
#     number_of_children = models.IntegerField(null=True, blank=True)
#     is_paid = models.BooleanField(default=False)

class Aeroport(models.Model):
    u"""Модель аэропорта назначения, уникальная для каждого сайта"""

    lowcost_site = models.ForeignKey(LowcostSite)
    code = models.CharField(max_length=10, verbose_name=u'Код аэропорта', unique=True)
    name = models.CharField(max_length=125, verbose_name=u'Название аэропорта')
    short_name = models.CharField(max_length=125, verbose_name=u'Краткое название аэропорта')

    def __unicode__(self): return u'{0} {1}'.format(self.dest_code, self.dest_name) 

    class Meta:
        ordering = ['code', 'name']

class AeroportsRelations(models.Model):
    u"""Модель отображения возможных полетов из аэропорта."""

    from_dest = models.ForeignKey(Aeroport, 'code', related_name='aeroport')
    can_fly_to = models.ForeignKey(Aeroport, 'code', related_name='destination')
    timestamp = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.timestamp = datetime.datetime.utcnow().replace(tzinfo=utc)
        return super(AeroportsRelations, self).save(*args, **kwargs)